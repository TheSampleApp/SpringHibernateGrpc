package com.the.sample.app.grpc;

import com.google.protobuf.Empty;
import com.the.sample.app.model.User;
import com.the.sample.app.service.UserService;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import org.lognet.springboot.grpc.GRpcService;

import static com.the.sample.app.grpc.UserServiceEndpointGrpc.UserServiceEndpointImplBase;
import static com.the.sample.app.grpc.UserServiceEndpointOuterClass.UserByIdRequest;
import static com.the.sample.app.grpc.UserServiceEndpointOuterClass.UserResponse;
import static com.the.sample.app.grpc.UserServiceEndpointOuterClass.CreateUserRequest;
import static com.the.sample.app.grpc.UserServiceEndpointOuterClass.UpdateUserRequest;
import static com.the.sample.app.grpc.UserServiceEndpointOuterClass.UserDto;
import static com.the.sample.app.grpc.UserServiceEndpointOuterClass.UserByEmailRequest;
import java.util.Optional;

@GRpcService
@AllArgsConstructor
public class UserServiceEndpointImpl extends UserServiceEndpointImplBase {
    private final UserService userService;

    @Override
    public void findById(UserByIdRequest request, StreamObserver<UserResponse> responseObserver) {
        UserDto userDto = Optional.ofNullable(request).map(UserByIdRequest::getId).
                map(userService::findById).
                flatMap(res ->
                res.map(userData->UserDto.newBuilder().
                        setEmail(userData.getEmail()).
                        setFullName(userData.getFullName()).build())).orElse(null);
        responseObserver.onNext(UserResponse.newBuilder().setUser(userDto).build());
        responseObserver.onCompleted();
    }

    @Override
    public void findByEmail(UserByEmailRequest request, StreamObserver<UserResponse> responseObserver) {
        UserDto userDto = Optional.ofNullable(request).map(UserByEmailRequest::getEmail).
                map(userService::findByEmail).
                flatMap(res ->
                        res.map(userData->UserDto.newBuilder().
                                setEmail(userData.getEmail()).
                                setFullName(userData.getFullName()).build())).orElse(null);
        responseObserver.onNext(UserResponse.newBuilder().setUser(userDto).build());
        responseObserver.onCompleted();
    }

    @Override
    public void save(CreateUserRequest request, StreamObserver<Empty> responseObserver) {
        Optional.ofNullable(request).ifPresent(req ->
                userService.save(User.builder().
                    email(req.getEmail()).
                    fullName(req.getFullName()).build()));
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void update(UpdateUserRequest request, StreamObserver<Empty> responseObserver) {
        Optional.ofNullable(request).ifPresent(req ->
                userService.save(User.builder().
                        id(req.getId()).
                        email(req.getEmail()).
                        fullName(req.getFullName()).build()));
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }

    @Override
    public void deleteById(UserByIdRequest request, StreamObserver<Empty> responseObserver) {
        Optional.ofNullable(request).
                map(UserByIdRequest::getId).
                ifPresent(id->userService.deleteById(id));
        responseObserver.onNext(Empty.newBuilder().build());
        responseObserver.onCompleted();
    }
}
