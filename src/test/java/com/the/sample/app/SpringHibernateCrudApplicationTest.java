package com.the.sample.app;

import com.the.sample.app.model.User;
import com.the.sample.app.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = SpringHibernateCrudApplication.class)
public class SpringHibernateCrudApplicationTest {
    @Autowired
    private UserService userService;

    @Test
    void testCreateAndFind(){
        userService.save(User.builder().
                fullName("test user").
                email("test@test.com").
                build());
        assertTrue(userService.findByEmail("test@test.com").isPresent());
    }
}
